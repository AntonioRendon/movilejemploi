package comvg.example.cursosandroid.micalculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    Button btnSuma, btnResta,btnDivi,btnMult,btnLimpiar,btnCerrar;
    EditText txtNum1, txtNum2,txtResult;
    Operaciones op = new Operaciones();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnSuma = findViewById(R.id.btnSuma);
        btnResta = findViewById(R.id.btnResta);
        btnMult = findViewById(R.id.btnMult);
        btnDivi = findViewById(R.id.btnDivi);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnCerrar = findViewById(R.id.btnCerrar);
    }
    public void setEventos(){

        this.btnSuma.setOnClickListener(this);
        this.btnResta.setOnClickListener(this);
        this.btnDivi.setOnClickListener(this);
        this.btnMult.setOnClickListener(this);
        this.btnLimpiar.setOnClickListener(this);
        this.btnCerrar.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {

        switch( view.getId()){

            case  R.id.btnSuma:
                if(txtNum1.getText().toString().matches("") ||  txtNum2.getText().toString().matches("")){
                    Toast.makeText(MainActivity.this,"Faltan datos: ", Toast.LENGTH_SHORT).show();}
                else {
                    op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                    op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                    txtResult.setText("" + op.suma());
                }
                break;
            case R.id.btnResta:
                if(txtNum1.getText().toString().matches("") ||  txtNum2.getText().toString().matches("")){
                    Toast.makeText(MainActivity.this,"Faltan datos ", Toast.LENGTH_SHORT).show();
                }
                else {

                    op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                    op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                    txtResult.setText("" + op.resta());
                }
                break;
            case R.id.btnMult:
                if(txtNum1.getText().toString().matches("") ||  txtNum2.getText().toString().matches("")){

                    Toast.makeText(MainActivity.this,
                            "Faltan datos  ", Toast.LENGTH_SHORT).show();}
                else {

                    op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                    op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                    txtResult.setText("" + op.mult());
                }
                break;
            case R.id.btnDivi:
                if(txtNum1.getText().toString().matches("") ||  txtNum2.getText().toString().matches("")){

                    Toast.makeText(MainActivity.this,
                            "Faltan datos  ", Toast.LENGTH_SHORT).show();}
                else {

                    op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                    op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                    txtResult.setText("" + op.div());
                }
                break;
            case R.id.btnLimpiar:
                txtNum1.setText("");
                txtNum2.setText("");
                txtResult.setText("");

                break;
            case R.id.btnCerrar:
                finish();
                break;

        }

    }
}
